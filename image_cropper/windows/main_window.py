from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QWidget

from image_cropper.windows.cropper_window import CropperWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("windows/ui/main.ui", self)

        self.cropper_window = CropperWindow()
        self.start_push_button.clicked.connect(self.showNextWindow)

    def showNextWindow(self):
        self.hide()
        self.cropper_window.show()
