from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow


class CropperWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("windows/ui/cropper.ui", self)
