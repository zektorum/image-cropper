import logging
import sys

from PyQt5.QtWidgets import QApplication

from windows.main_window import MainWindow


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    app.exec()
